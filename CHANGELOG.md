# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

## [0.1.6] 20 Feb 2017
- Added `readOnly` validation error.

## [0.1.5] 10 Feb 2017
- Added `invalid` validation error. 

## [0.1.4] 20 Sep 2016
- Added chaining to `ValidationErrors` methods.
- Updated dev deps.

## [0.1.3] 20 Sep 2016
- Added `notFound` validation error.

## [0.1.2] 19 Sep 2016
- Added `getErrors` method to `Errors` class.

## [0.1.1] 16 Sep 2016
- Added `unique` validation error.

## [0.1.0] 15 Sep 2016
- Initial release.
