/**
 * @copyright 2016 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import {Errors} from './Errors';

/**
 * Construct a validation error object.
 *
 * @param {string} property - The name of the property that was validated.
 * @param {string} type     - The code for the error, usually in a form that can be translated into human-readable text.
 * @param {*}      expects  - The expected value or constraint.
 * @returns {{property: string, type: string, expects: any}}
 */
function error(property: string, type: string, expects?: any): any {
	return {
		property: property,
		type: type,
		expects: expects
	};
}

/**
 * A validation error list class.
 */
export class ValidationErrors extends Errors {
	/**
	 * Add an error for a required property.
	 *
	 * @param {string} property - The name of the property.
	 * @returns {this}
	 */
	public required(property: string): this {
		return this.push(error(property, 'required'));
	}

	/**
	 * Add an error for a invalid property.
	 *
	 * @param {string} property - The name of the property.
	 * @returns {this}
	 */
	public invalid(property: string): this {
		return this.push(error(property, 'invalid'));
	}

	/**
	 * Add an error for a read-only property.
	 *
	 * @param {string} property - The name of the property.
	 * @returns {this}
	 */
	public readOnly(property: string): this {
		return this.push(error(property, 'read-only'));
	}

	/**
	 * Add an error for a property that requires a unique value.
	 *
	 * @param {string} property - The name of the property.
	 * @returns {this}
	 */
	public unique(property: string): this {
		return this.push(error(property, 'unique'));
	}

	/**
	 * Add an error for a property that requires relational integrity.
	 *
	 * @param {string} property - The name of the property.
	 * @returns {this}
	 */
	public notFound(property: string): this {
		return this.push(error(property, 'not_found'));
	}

	/**
	 * Add an error for a string that is too short.
	 *
	 * @param {string} property - The name of the property.
	 * @param {number} expects  - The minimum length that is expected for the string.
	 * @returns {this}
	 */
	public minLength(property: string, expects: number): this {
		return this.push(error(property, 'length.minimum', expects));
	}

	/**
	 * Add an error for a string that is too long.
	 *
	 * @param {string} property - The name of the property.
	 * @param {number} expects  - The maximum length that is expected for the string.
	 * @returns {this}
	 */
	public maxLength(property: string, expects: number): this {
		return this.push(error(property, 'length.maximum', expects));
	}
}
