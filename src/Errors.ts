/**
 * @copyright 2016 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

/**
 * A generic error list class.
 */
export class Errors {
	private errors: any[];

	/**
	 * @param {object} [errors] - Any number of arguments can be used to prime the error list.
	 */
	constructor(...errors: any[]) {
		this.errors = errors || [];
	}

	/**
	 * Push an error onto the list.
	 *
	 * @param {object} error - An object representing an error.
	 * @returns {this}
	 */
	public push(error: any): this {
		this.errors.push(error);

		return this;
	}

	/**
	 * Determine if any errors have been added to the object.
	 *
	 * @returns {boolean} - Returns true if there are errors in the list, false otherwise.
	 */
	public any(): boolean {
		return this.errors.length > 0;
	}

	/**
	 * Gets the entire list of errors.
	 *
	 * @returns {any[]}
	 */
	public getErrors(): any[] {
		return this.errors;
	}

	/**
	 * Converts the error list to JSON, anchoring the list to a "$errors" property so that API's can detect the errors.
	 *
	 * @returns {{$errors: any[]}}
	 */
	public toJSON(): any {
		return {
			$errors: this.errors
		}
	}
}
