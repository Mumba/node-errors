[![build status](https://gitlab.com/Mumba/node-errors/badges/master/build.svg)](https://gitlab.com/Mumba/node-errors/commits/master)
[![coverage report](https://gitlab.com/Mumba/node-errors/badges/master/coverage.svg)](https://gitlab.com/Mumba/node-errors/commits/master)
## Mumba Errors

A set of classes for managing lists of errors.

## Installation

```sh
$ npm install --save mumba-errors
```

## Examples

```typescript
import {ValidationErrors} from 'mumba-errors';

let data = {
	title: 'foo'
};
let errors = new ValidationErrors();

if (data.title.length < 8) {
	errors.minLength('title', 8);
}

if (errors.any()) {
	console.log(JSON.stringify(errors, null, 2));
}
```

Outputs:

```json
{
  "$errors": [
    {
      "property": "title",
      "type": "length.minimum",
      "expects": 8
    }
  ]
}
```

## Tests

To run the test suite, first install the dependencies, then run `npm test`:

```sh
$ npm install
$ npm test
```

## People

The original author of _Mumba Errors_ is [Andrew Eddie](https://gitlab.com/u/aeddie.mumba).

[List of all contributors](https://gitlab.com/Mumba/node-errors/graphs/master)

## License

[Apache 2.0](LICENSE.txt)

* * *

&copy; 2016 [Mumba Pty Ltd](http://www.mumba.cloud). All rights reserved.
