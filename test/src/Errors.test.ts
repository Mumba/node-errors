/**
 * Remember to observe the 4 A's of testing and try to limit tests to just four calls:
 * - Arrange    - set up the system state
 * - Act        - do the thing we are testing
 * - Assert     - inspect the resulting state
 * - Annihilate - tear down
 *
 * @copyright 2016 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import * as assert from "assert";
import {Errors} from '../../src/index';

describe('Errors unit tests', () => {
	it('should push errors onto the internal stack', () => {
		let errors = new Errors();

		assert.strictEqual(errors.push(new Error('foo')), errors, 'should chain');

		assert.equal(errors.toJSON().$errors[0].message, 'foo', 'should be the error');
	});

	it('.any should return false if no errors', () => {
		let errors = new Errors();

		assert.strictEqual(errors.any(), false, 'should return false')
	});

	it('.any should return true if there are any errors', () => {
		let errors = new Errors([new Error('foo')]);

		assert.strictEqual(errors.any(), true, 'should return true');
	});

	it('should get a list of the errors', () => {
		let errors = new Errors({ foo: 'bar' });

		assert.deepEqual(errors.getErrors(), [
			{
				foo: 'bar'
			}
		], 'should be the array of errors');

	});

	it('should export errors to a JSON friendly object', () => {
		let errors = new Errors({ foo: 'bar' });

		assert.deepEqual(errors.toJSON(), {
			$errors: [
				{
					foo: 'bar'
				}
			]
		}, 'should indent errors array using "$errors" property');
	});
});
