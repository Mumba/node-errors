/**
 * Remember to observe the 4 A's of testing and try to limit tests to just four calls:
 * - Arrange    - set up the system state
 * - Act        - do the thing we are testing
 * - Assert     - inspect the resulting state
 * - Annihilate - tear down
 *
 * @copyright 2016 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import * as assert from "assert";
import {ValidationErrors} from '../../src/index';

describe('ValidationErrors', () => {
	it('should add a required error', () => {
        let errors = new ValidationErrors();

		assert.strictEqual(errors.required('title'), errors, 'should chain');

		assert.equal(errors.toJSON().$errors[0].property, 'title', 'should be the property');
		assert.equal(errors.toJSON().$errors[0].type, 'required', 'should be the type');
		assert.strictEqual(errors.toJSON().$errors[0].expects, void 0, 'should ignore expects');
	});

	it('should add a unique error', () => {
        let errors = new ValidationErrors();

		assert.strictEqual(errors.unique('title'), errors, 'should chain');

		assert.equal(errors.toJSON().$errors[0].property, 'title', 'should be the property');
		assert.equal(errors.toJSON().$errors[0].type, 'unique', 'should be the type');
		assert.strictEqual(errors.toJSON().$errors[0].expects, void 0, 'should ignore expects');
	});

	it('should add a not found error', () => {
        let errors = new ValidationErrors();

		assert.strictEqual(errors.notFound('id'), errors, 'should chain');

		assert.equal(errors.toJSON().$errors[0].property, 'id', 'should be the property');
		assert.equal(errors.toJSON().$errors[0].type, 'not_found', 'should be the type');
		assert.strictEqual(errors.toJSON().$errors[0].expects, void 0, 'should ignore expects');
	});

	it('should add a string too short error', () => {
		let errors = new ValidationErrors();

		assert.strictEqual(errors.minLength('title', 8), errors, 'should chain');

		assert.equal(errors.toJSON().$errors[0].property, 'title', 'should be the property');
		assert.equal(errors.toJSON().$errors[0].type, 'length.minimum', 'should be the type');
		assert.equal(errors.toJSON().$errors[0].expects, 8, 'should have expects');
	});

	it('should add a string too long error', () => {
		let errors = new ValidationErrors();

		assert.strictEqual(errors.maxLength('title', 255), errors, 'should chain');

		assert.equal(errors.toJSON().$errors[0].property, 'title', 'should be the property');
		assert.equal(errors.toJSON().$errors[0].type, 'length.maximum', 'should be the type');
		assert.equal(errors.toJSON().$errors[0].expects, 255, 'should have expects');
	});

	it('should add an invalid error', () => {
		let errors = new ValidationErrors();

		assert.strictEqual(errors.invalid('email'), errors, 'should chain');

		assert.equal(errors.toJSON().$errors[0].property, 'email', 'should be the property');
		assert.equal(errors.toJSON().$errors[0].type, 'invalid', 'should be the type');
	});

	it('should add a read-only error', () => {
		let errors = new ValidationErrors();

		assert.strictEqual(errors.readOnly('createdBy'), errors, 'should chain');

		assert.equal(errors.toJSON().$errors[0].property, 'createdBy', 'should be the property');
		assert.equal(errors.toJSON().$errors[0].type, 'read-only', 'should be the type');
	});

	it('should add a string requires number error');
	it('should add a string requires uppercase error');
	it('should add a string requires lowercase error');
	it('should add a string requires special error');
	it('should add a range too low error');
	it('should add a range too high error');
});
